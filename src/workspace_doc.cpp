#include "workspace.h"
#include "workspace_impl.h"

#include "document.h"

#include <QGuiApplication>

#include <QAction>
#include <QDir>
#include <QDockWidget>
#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>
#include <QWidget>

#include <unordered_set>


bool Workspace::saveDocument(QObject *doc, bool saveAs)
{
    Q_ASSERT(m_documents.contains(doc));

    auto &info = m_documents[doc];
    if(auto const &it = m_documentWrirers.find(info.moniker);it != m_documentWrirers.end())
    {
        auto *writer = it->second;

        if(saveAs || info.url.isEmpty())
        {
            QString saveDir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
            if(auto const &it = m_lastSaveDirs.find(info.moniker);it != m_lastSaveDirs.end())
                saveDir = it->second;

            QString selectedFilter;
            auto const filePath = QFileDialog::getSaveFileName(mainWindow(), tr("Save Document"),
                                                               saveDir, writer->filterText(info.moniker), &selectedFilter);
            if(filePath.isEmpty())
                return false;

            info.url = filePath;
            m_lastSaveDirs[info.moniker] = QFileInfo(filePath).absoluteDir().absolutePath();
        }

        if(QFile file(info.url);file.open(QFile::WriteOnly))
        {
            qDebug() << "Writing document to file:" << info.url;
            if(auto errcode = writer->write(doc, &file, info.moniker);errcode != 0)
            {
                QMessageBox::critical(mainWindow(), tr("Save File Error"), QString(tr("Error %1")).arg(errcode));
                return false;
            }

            return true;
        }
        else
            QMessageBox::critical(mainWindow(), tr("File Error"), QString(tr("Cannot open file %1 for writing")).arg(info.url));
    }

    return false;
}

void Workspace::onDocumentNew()
{
    if(auto *action = qobject_cast<QAction *>(sender()))
    {
        auto const &key = action->data().toString();
        if(auto it=m_documentFactories.find(key);it != m_documentFactories.end())
        {
            auto [_, factory] = *it;
            auto const &moniker = key.section(':', -2, -1);

            if(auto *doc = factory->createDocument(moniker))
            {
                if(auto *widget = factory->createEditor(moniker, mainWindow()))
                {
                    if(setupDocumentWindow(moniker, doc, widget))
                        return;
                    delete widget;
                }

                delete doc;
            }
        }
    }
}

void Workspace::onDocumentOpen()
{
    auto const serializers = afx::queryInterfaces<afx::IDocumentSerializer>();

    std::map<QString, std::pair<afx::IDocumentFactory *, afx::IDocumentSerializer *>> monikerMap;
    std::map<QString, QString> filterMap;
    QStringList filters;
    for(auto *factory : afx::queryInterfaces<afx::IDocumentFactory>())
        for(auto const &p : factory->documentMonikers())
        {
            auto const &moniker = normalizeMoniker(p);
            for(auto *ser : serializers)
                if(ser->canRead(moniker))
                {
                    if(auto const &filt = ser->filterText(moniker);!filt.isEmpty())
                    {
                        monikerMap.insert({ moniker, { factory, ser } });
                        filterMap.insert({ filt, moniker });
                        filters.push_back(filt);
                        break;
                    }
                }
        }


    QString selectedFilter;
    auto const file = QFileDialog::getOpenFileName(
        mainWindow(),
        tr("Open"),
        [](auto *settings){ return settings->value(s_keyLastOpenFileDir).toString(); }(settings()),
        filters.join(";;"), &selectedFilter);
    if(!file.isEmpty())
    {
        settings()->setValue(s_keyLastOpenFileDir, QFileInfo(file).absoluteFilePath());

        if(QFile f(file);f.open(QIODevice::ReadOnly) && filterMap.contains(selectedFilter))
        {
            auto const &moniker = filterMap[selectedFilter];
            auto const &[factory, serializer] = monikerMap[moniker];

            if(auto *doc = factory->createDocument(moniker))
            {
                Q_ASSERT(qobject_cast<afx::IDocument *>(doc));
                if(!serializer->read(doc, &f, moniker))
                {
                    if(auto *widget = factory->createEditor(moniker, mainWindow()))
                    {
                        if(setupDocumentWindow(moniker, doc, widget))
                            return;
                        delete widget;
                    }

                    delete doc;
                }
            }
        }
    }
}

void Workspace::onDocumentOpenRecent()
{
    qDebug() << "Workspace::onDocumentOpenRecent: not implemented";
}

void Workspace::onDocumentSave()
{
    if(auto *doc = activeDocument())
        saveDocument(doc);
}

void Workspace::onDocumentSaveAs()
{
    if(auto *doc = activeDocument())
        saveDocument(doc, true);
}

void Workspace::onDocumentSaveAll()
{
    qDebug() << "Workspace::onDocumentSaveAll: not implemented";
}

void Workspace::onDocumentCleanStateChanged()
{
    bool isDirty = false;
    for(auto const &[obj, _] : m_documents)
        if(auto *doc = qobject_cast<afx::IDocument *>(obj); !doc->isClean())
        {
            isDirty = true;
            break;
        }

    if(m_documentSaveAction)
        m_documentSaveAction->setEnabled(isDirty);
    if(m_documentSaveAllAction)
        m_documentSaveAllAction->setEnabled(isDirty);
}

void Workspace::onDocumentDestroy()
{
    Q_ASSERT(m_documents.contains(sender()));
    m_documents.erase(sender());
}
