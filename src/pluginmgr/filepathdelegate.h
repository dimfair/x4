#ifndef AFXFILEPATHDELEGATE_H
#define AFXFILEPATHDELEGATE_H

#include <QStyledItemDelegate>

class AfxFilePathDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    AfxFilePathDelegate(QObject *parent = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
    void onSelectFolder();
};


#endif // AFXFILEPATHDELEGATE_H
