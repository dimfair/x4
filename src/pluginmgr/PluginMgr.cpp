#include "PluginMgr.h"

#include "filepathdelegate.h"

#include <QSettings>

namespace afx {
namespace plugins {

constexpr const char s_keyLocations[] = "Plugins/Locations";

PluginMgr::PluginMgr(QObject *parent)
    : QObject(parent)
{
}

QString PluginMgr::name() const
{
    return "PluginMgr";
}

QString PluginMgr::description() const
{
    return "Plugin Manager";
}

bool PluginMgr::initialize()
{
    if(auto *se = afx::queryInterface<afx::ISettingsEditor>())
    {
        se->registerValueListForm(tr("Plugins/Locations"), s_keyLocations, new AfxFilePathDelegate(this));
        return true;
    }

    return false;
}

void PluginMgr::loadState(QSettings *)
{
}

void PluginMgr::saveState(QSettings *) const
{
}

QStringList PluginMgr::pluginPaths()
{
    if(auto *settings = afx::queryInterface<QSettings>())
    {
        auto key = QString("%1/%2").arg(s_keyLocations, "values");
        auto var = settings->value(key);
        return var.toStringList();
    }
    return {};
}

}
}
