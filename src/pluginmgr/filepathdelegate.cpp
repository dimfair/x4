#include "filepathdelegate.h"

#include <QLineEdit>
#include <QFileDialog>

AfxFilePathDelegate::AfxFilePathDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
}

QWidget *AfxFilePathDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    auto *editor = new QLineEdit(parent);
    auto *action = editor->addAction(QIcon::fromTheme("folder"), QLineEdit::TrailingPosition);

    action->setData(QVariant::fromValue(QPersistentModelIndex(index)));
    connect(action, &QAction::triggered, this, &AfxFilePathDelegate::onSelectFolder);

    return editor;
}

void AfxFilePathDelegate::setEditorData(QWidget *w, const QModelIndex &index) const
{
    auto *editor = static_cast<QLineEdit *>(w);
    auto const &text = index.model()->data(index, Qt::EditRole).toString();
    editor->setText(text);
}

void AfxFilePathDelegate::setModelData(QWidget *w, QAbstractItemModel *model, const QModelIndex &index) const
{
    auto *editor = static_cast<QLineEdit *>(w);
    auto const &text = editor->text();
    model->setData(index, text, Qt::EditRole);
}

void AfxFilePathDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
    editor->setGeometry(option.rect);
}

void AfxFilePathDelegate::onSelectFolder()
{
    if(auto *action = qobject_cast<QAction *>(sender()))
    {
        auto var = action->data();
        if(auto const dir = QFileDialog::getExistingDirectory(); !dir.isEmpty())
        {
            if(QModelIndex index = var.value<QPersistentModelIndex>();index.isValid())
            {
                auto *model = const_cast<QAbstractItemModel *>(index.model()); // Meow...
                model->setData(index, dir);
            }
        }
    }
}
