#ifndef SPREADSHEET_H_202309141633
#define SPREADSHEET_H_202309141633

#include <QObject>

#include "../workspace.h"


namespace afx {
namespace plugins {

class PluginMgr : public QObject,
                  public afx::IPlugin,
                  public afx::IPluginMgr
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.xvier-project.Plugins.PluginMgr" FILE "PluginMgr.json")
    Q_INTERFACES( afx::IPlugin  afx::IPluginMgr )

public:
    PluginMgr(QObject * = {});
   ~PluginMgr() = default;

    // afx::IPlugin overrides
    QString name() const override;
    QString description() const override;
    bool initialize() override;
    void loadState(QSettings *) override;
    void saveState(QSettings *) const override;

    // afx::IPluginMrg overrides
    QStringList pluginPaths() override;
};

}   // plugins
}   // afx

#endif // SPREADSHEET_H_202309141633
