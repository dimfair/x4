#include "workspace.h"
#include "workspace_impl.h"

#include "document.h"
#include "actionbldr.h"

#include <QGuiApplication>

#include <QAction>
#include <QDir>
#include <QDockWidget>
#include <QFile>
#include <QFileDialog>
#include <QIcon>
#include <QMainWindow>
#include <QMessageBox>
#include <QMdiArea>
#include <QMenu>
#include <QMenuBar>
#include <QSettings>
#include <QStandardPaths>
#include <QMdiSubWindow>
#include <QToolBar>
#include <QToolButton>

#include <QCloseEvent>
#include <QWindowStateChangeEvent>

#include <QObject>
#include <QPluginLoader>

#include <unordered_set>

const QString Workspace::s_keyMainWindow = QStringLiteral("MainWindow/");
const QString Workspace::s_keyLastOpenFileDir = QStringLiteral("lastOpenFileDir");

QString Workspace::normalizeMoniker(const QString &s)
{
    QStringList result;
    for(auto const &list = s.split(':');auto const &part : list)
        result.push_back(part.simplified());
    return result.join(':');
}

namespace afx {

class IDocumentFactory;
class IDocumentSerializer;

IWorkspace *createMdiWorkspace(QGuiApplication *app)
{
    return new Workspace(true, app);
}

}

Workspace::Workspace(bool isMdi, QGuiApplication *theApp)
    : QObject(theApp)
{
    Q_ASSERT(QCoreApplication::instance());
    setObjectName("AfxWorkspace");

    if(isMdi)
    {
        m_mdiArea = new QMdiArea;
        m_mdiArea->installEventFilter(this);
        connect(m_mdiArea, &QMdiArea::subWindowActivated, this, &Workspace::onSubWindowActivated);
    }

    connect(theApp, &QGuiApplication::applicationStateChanged, this, &Workspace::onApplicationStateChanged);
}

Workspace::~Workspace()
{
}

QMainWindow *Workspace::mainWindow()
{
    if(!m_mainWnd)
    {
        m_mainWnd = new QMainWindow;
        m_mainWnd->setObjectName("AfxMainWindow");
        m_mainWnd->installEventFilter(this);

        if(m_mdiArea)
            m_mainWnd->setCentralWidget(m_mdiArea);

        m_mainMenuBar = new QMenuBar(m_mainWnd);
        m_mainMenuBar->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);

        m_mainToolBar = new QToolBar(m_mainWnd);
        m_mainToolBar->setAllowedAreas(Qt::TopToolBarArea);
        m_mainToolBar->setMovable(false);
        m_mainWnd->addToolBar(Qt::TopToolBarArea, m_mainToolBar);

        m_burgerButton = new QToolButton(m_mainWnd);
        m_burgerButton->setIcon(QIcon::fromTheme("application-menu"));
        m_burgerButton->setPopupMode(QToolButton::InstantPopup);

        m_humburgerAction = m_mainToolBar->addWidget(m_burgerButton);
        m_mainMenuAction = m_mainToolBar->addWidget(m_mainMenuBar);
//        m_humburgerAction->setVisible(false);
//        m_mainMenuAction->setVisible(false);

        // it allowes to access to main window using queryInterface
        m_plugins.push_back(m_mainWnd);
    }

    return m_mainWnd;
}

QSettings *Workspace::settings()
{
    if(!m_settings)
    {
        m_settings = new QSettings(this);
        m_settings->setObjectName("AfxSettings");
        m_settings->installEventFilter(this);
    }

    return m_settings;
}

void Workspace::loadPlugins(const QStringList &paths)
{
    auto initPlugins = [this](std::unordered_set<QObject *>  &pluginsLoaded){
        const int attempts = pluginsLoaded.size()*pluginsLoaded.size() + 1;
        for(int n=1;!pluginsLoaded.empty() && n <= attempts;n++)
            for(auto it=pluginsLoaded.begin();it != pluginsLoaded.end();)
            {
                auto *obj = *it;
                if(auto *plugin = qobject_cast<afx::IPlugin *>(obj))
                {
                    if(plugin->initialize())
                    {
                        qDebug() << "[Workspace::loadPlugins]" << plugin->name() << "initialized.";
                        m_plugins.push_back(obj);
                        it = pluginsLoaded.erase(it);
                    }
                    else
                    {
                        qDebug() << "[Workspace::loadPlugins]" << plugin->name() << "initialization failed, attempt" << n << "/" << attempts;
                        ++it;
                    }
                }
                else
                {
                    qDebug() << "[Workspace::loadPlugins]" << "unknown plugin" << obj;
                    m_plugins.push_back(obj);
                    it = pluginsLoaded.erase(it);
                }
            }

    };

    auto *settings = this->settings();
    std::unordered_set<QObject *> pluginsLoaded;

    // it allowes to access to settings using queryInterface
    m_plugins.push_back(settings);

    // load static plugins first
    for(auto *obj : QPluginLoader::staticInstances())
    {
        qDebug() << "[Workspace::loadPlugins] static pluging loaded:" << obj->objectName();
        obj->setParent(this);
        pluginsLoaded.insert(obj);
    }

    initPlugins(pluginsLoaded);


    auto locations = paths;
    if(auto *pluginMgr = afx::queryInterface<afx::IPluginMgr>())
        locations += pluginMgr->pluginPaths();

    QSet<QString> loadedFiles;
    foreach(const QString &path, locations)
    {
        qDebug() << "[Workspace::loadPlugins] looking for plugins in:" << path;

        QDir pluginDir(path);
        for(auto const &fileName : pluginDir.entryList(QDir::Files))
            if(!loadedFiles.contains(fileName))
            {
                QPluginLoader loader(pluginDir.absoluteFilePath(fileName));
                if(auto *plugin = loader.instance())
                {
                    qDebug() << "[Workspace::loadPlugins] plugin loaded:" << pluginDir.absoluteFilePath(fileName);

                    plugin->setParent(this);
                    pluginsLoaded.insert(plugin);
                    loadedFiles.insert(fileName);
                }
                else
                    qDebug() << loader.errorString();
            }
            else
                qDebug() << "Attempt to load plugin twice" << fileName;
    }

    initPlugins(pluginsLoaded);

    if(!pluginsLoaded.empty())
    {
        QStringList list;
        for(auto *obj : pluginsLoaded)
            if(auto *plugin = qobject_cast<afx::IPlugin *>(obj))
                list.push_back(plugin->name());
        qCritical() << "[AfxMainWindow::loadPlugins] plugins unloaded:" << list.join(", ");
    }

    for(auto *obj : m_plugins)
        if(auto *p = qobject_cast<afx::IPlugin *>(obj))
            p->loadState(settings);
}

QObjectList Workspace::plugins() const
{
    return m_plugins;
}

void Workspace::registerDocumentFactory(afx::IDocumentFactory *factory)
{
    for(auto const &moniker : factory->documentMonikers())
        m_documentFactories[ normalizeMoniker(moniker) ] = factory;
}

void Workspace::registerDocumentSerializer(afx::IDocumentSerializer *ser)
{
    for(auto *factory : afx::queryInterfaces<afx::IDocumentFactory>())
        for(auto const &moniker : factory->documentMonikers())
        {
            if(ser->canRead(moniker))
                m_documentReaders[ normalizeMoniker(moniker) ] = ser;
            if(ser->canWrite(moniker))
                m_documentWrirers[ normalizeMoniker(moniker) ] = ser;
        }
}

std::map<QString, QAction *> Workspace::actions() const
{
    std::map<QString, QAction *> actions;
    for(auto *action : findChildren<QAction *>(QString{}, Qt::FindDirectChildrenOnly))
        actions[action->objectName()] = action;
    return actions;
}

afx::ActionBuilder Workspace::createAction(const char *name, QObject *parent)
{
    auto slotName = [](const char *name){
        std::string slotName = "1on";

        bool flag = true;
        for(auto const *p = name;*p;++p)
        {
            if(*p == '-')
            {
                flag = true;
                continue;
            }

            slotName.push_back(flag ? std::toupper(*p) : (*p));
            flag = false;
        }

        return slotName + "()";
    };

    auto *action = new QAction(parent ? parent : this);

    action->setObjectName(name);
    action->setIcon(QIcon::fromTheme(name));
    auto conn = connect(action, SIGNAL(triggered()), this, slotName(name).c_str());

    action->setEnabled(conn);
    return afx::ActionBuilder(action);
}

QIcon Workspace::loadIcon(const char *name)
{
    return QIcon::fromTheme(name);
}

afx::MainWindowLayout Workspace::mainWindowLayout() const
{
    return m_mainWindowLayout;
}

void Workspace::setMainWindowLayout(afx::MainWindowLayout mainWindowLayout)
{
    if(m_mainWindowLayout != mainWindowLayout)
    {
        m_mainWindowLayout = mainWindowLayout;

        switch(mainWindowLayout)
        {
        case afx::MainWindowLayout::Classic:
            mainWindow()->menuBar()->setVisible(true);
            m_humburgerAction->setVisible(false);
            m_mainMenuAction->setVisible(false);
            m_mainToolBar->setVisible(false);
            break;
        case afx::MainWindowLayout::Compact:
            mainWindow()->menuBar()->setVisible(false);
            m_humburgerAction->setVisible(false);
            m_mainMenuAction->setVisible(true);
            m_mainToolBar->setVisible(true);
            break;
        case afx::MainWindowLayout::HamburgerMenu:
            mainWindow()->menuBar()->setVisible(false);
            m_humburgerAction->setVisible(true);
            m_mainMenuAction->setVisible(false);
            m_mainToolBar->setVisible(true);
            break;
        }
    }
}

void Workspace::show(afx::MainWindowLayout mainWindowLayout)
{
    auto *win = mainWindow();
    if(!m_isMainWndInitialized)
    {
        m_isMainWndInitialized = true;
        initializeMainWnd();
        setMainWindowLayout(mainWindowLayout);
    }

    win->show();
}

bool Workspace::eventFilter(QObject *watchedObj, QEvent *ev)
{
    if(watchedObj == m_mainWnd)
        return processMainWndEvent(ev);
    if(m_mdiWigdetMap.contains(static_cast<QMdiSubWindow *>(watchedObj)))
        return processMdiSubwindowEvent(ev, static_cast<QMdiSubWindow *>(watchedObj));
    return false;
}

bool Workspace::processMainWndEvent(QEvent *ev)
{
    auto processCloseEvent = [this](auto *ev) {
        auto *settings = this->settings();
        for(auto *obj : m_plugins)
            if(auto *p = qobject_cast<afx::IPlugin *>(obj))
                p->saveState(settings);
        saveMainWindowState(settings);
        ev->accept();
        return false;
    };
    auto processResizeEvent = [](auto *) {
        return false;
    };
    auto processWindowStateChangeEvent = [](auto *) {
        return false;
    };

    switch(ev->type())
    {
    case QEvent::Close:
        return processCloseEvent(static_cast<QCloseEvent *>(ev));
    case QEvent::Resize:
        return processResizeEvent(static_cast<QResizeEvent *>(ev));
    case QEvent::WindowStateChange:
        return processWindowStateChangeEvent(static_cast<QWindowStateChangeEvent *>(ev));

    default:
        return false;
    }
}

bool Workspace::processMdiSubwindowEvent(QEvent *ev, QMdiSubWindow *subWin)
{
    switch(ev->type())
    {
    case QEvent::Close:
        if(auto *editor = qobject_cast<afx::IDocumentEditor *>(subWin->widget()))
            return processDocumentCloseEvent(static_cast<QCloseEvent *>(ev), editor->document());

    default:
        return false;
    }
}

bool Workspace::processDocumentCloseEvent(QCloseEvent *ev, QObject *obj)
{
    auto *doc = qobject_cast<afx::IDocument *>(obj);
    if(!doc->isClean() && m_documents[obj].editors.size() == 1)
    {
        auto const answer = QMessageBox::question(mainWindow(),
            tr("Close Document"),
            tr("The document has been modified.\nDo you want to save your changes?"),
            QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
            QMessageBox::Save);

        switch (answer)
        {
        case QMessageBox::Save:
            ev->setAccepted(saveDocument(obj));
            return true;

        default:
            ev->setAccepted(answer == QMessageBox::Discard);
            return true;
        }
    }

    ev->accept();
    return true;
}

void Workspace::saveMainWindowState(QSettings *settings) const
{
    settings->beginGroup(s_keyMainWindow);
    settings->setValue("geometry", m_mainWnd->saveGeometry());
    settings->setValue("layout", m_mainWnd->saveState());
    settings->endGroup();
}

void Workspace::restoreMainWindowState(QSettings *settings)
{
    settings->beginGroup(s_keyMainWindow);
    const auto geometry = settings->value("geometry", QByteArray()).toByteArray();
    if (!geometry.isEmpty())
        m_mainWnd->restoreGeometry(geometry);
    m_mainWnd->restoreState(settings->value("layout").toByteArray());
    settings->endGroup();
}

void Workspace::initializeMainWnd()
{
    createActions();
    createMenus();

    m_mainToolBar->addSeparator();
    if(m_documentNewAction)
    {
        m_mainToolBar->addAction(m_documentNewAction);
        if(m_documentNewAction->menu())
            if(auto *button = qobject_cast<QToolButton *>(m_mainToolBar->widgetForAction(m_documentNewAction)))
                button->setPopupMode(QToolButton::InstantPopup);
    }
    if(m_documentOpenAction)
    {
        m_mainToolBar->addAction(m_documentOpenAction);
        if(auto *button = qobject_cast<QToolButton *>(m_mainToolBar->widgetForAction(m_documentOpenAction)))
        {
            auto *menu = new QMenu(button);
            menu->addAction("Text1");
            menu->addAction("Text2");
            button->setMenu(menu);
            button->setPopupMode(QToolButton::MenuButtonPopup);
        }
    }
    if(m_documentSaveAction)
    {
        m_mainToolBar->addAction(m_documentSaveAction);
        if(auto *button = qobject_cast<QToolButton *>(m_mainToolBar->widgetForAction(m_documentSaveAction)))
        {
            auto *menu = new QMenu(button);
            menu->addAction(m_documentSaveAsAction);
            menu->addAction(m_documentSaveAllAction);
            button->setMenu(menu);
            button->setPopupMode(QToolButton::MenuButtonPopup);
        }
    }

    for(auto const *name : { {}, ActionEditUndo, ActionEditRedo, {}, ActionEditCopy, ActionEditCut, ActionEditPaste })
        if(name)
        {
            auto *button = new QToolButton(m_mainToolBar);
            button->setIcon(QIcon::fromTheme(name));
            button->setEnabled(false);

            m_editActionButtons[name] = { button, {} };
            m_mainToolBar->addWidget(button);
        }
        else
            m_mainToolBar->addSeparator();

    m_mainToolBar->adjustSize();

    for(auto *p : m_plugins)
    {
        if(auto *uiBuilder = qobject_cast<afx::IUiBuilder *>(p))
        {
            const QWidgetList &dockedWidgets = uiBuilder->dockWidgets();
            if(!dockedWidgets.isEmpty())
            {
                auto *wnd = mainWindow();
                QDockWidget *first = 0;
                foreach(QWidget *w, dockedWidgets)
                {
                    QDockWidget *dock = new QDockWidget(wnd);

                    QString name = w->windowTitle();
                    if(name.isEmpty())
                        name = w->objectName();
                    if(name.isEmpty())
                        name = w->metaObject()->className();

                    dock->setWidget(w);
                    dock->setObjectName("dock" + name);
                    dock->setWindowTitle(w->windowTitle().isEmpty() ? name : w->windowTitle());
                    dock->setFeatures(QDockWidget::DockWidgetClosable |
                                      QDockWidget::DockWidgetMovable |
                                      QDockWidget::DockWidgetFloatable);
                    wnd->addDockWidget(Qt::RightDockWidgetArea, dock);

                    connect(w, &QWidget::windowTitleChanged, dock, &QDockWidget::setWindowTitle);

                    if(first)
                        wnd->tabifyDockWidget(first, dock);
                    first = dock;
                }
            }
        }
    }

    restoreMainWindowState(settings());
}

bool Workspace::setupDocumentWindow(const QString &moniker, QObject *doc, QWidget *widget)
{
    if(auto *idoc = qobject_cast<afx::IDocument *>(doc))
        if(auto *editor = qobject_cast<afx::IDocumentEditor *>(widget))
        {
            auto const isDocRegistered = m_documents.contains(doc);
            auto &info = m_documents[doc];

            editor->initialize(doc);

            info.document = idoc;
            info.editors.insert(widget);
            if(!isDocRegistered)
            {
                info.moniker = moniker;
                info.connCleanChange = idoc->cleanChanged([this, doc](bool){ onDocumentCleanStateChanged(); });
                connect(doc, &QObject::destroyed, this, &Workspace::onDocumentDestroy);
            }
            Q_ASSERT(info.moniker == moniker);

            auto eraseWidget = [this, doc, widget](){
                Q_ASSERT(m_documents.contains(doc));

                auto &info = m_documents[doc];
                Q_ASSERT(info.editors.contains(widget));
                info.editors.erase(widget);

                if(info.editors.empty())
                {
                    // m_documents.erase(doc) will be called on doc destroy
                    delete doc;
                }
            };
            connect(widget, &QWidget::destroyed, eraseWidget);

            if(m_mdiArea)
            {
                auto *subWnd = m_mdiArea->addSubWindow(widget);
                if(m_mdiArea->subWindowList().size() == 1)
                    subWnd->showMaximized();
                else
                    subWnd->show();

                m_mdiWigdetMap[subWnd] = doc;
                subWnd->installEventFilter(this);
            }

            return true;
        }

    return false;
}

void Workspace::createMenus()
{
    auto populateMenu = [this](auto *menu){
        for(auto *m : { m_fileMenu, m_editMenu, m_viewMenu, m_windowMenu, m_helpMenu })
            if(m)
                menu->addMenu(m);
    };

    m_fileMenu   = createFileMenu();
    m_editMenu   = createEditMenu();
    m_viewMenu   = createViewMenu();
    m_windowMenu = createWindowMenu();
    m_helpMenu   = createHelpMenu();

    populateMenu(mainWindow()->menuBar());
    mainWindow()->menuBar()->setVisible(true);

    if(m_burgerButton)
    {
        auto *burgerMainMnu = new QMenu(m_burgerButton);

        m_burgerButton->setMenu(burgerMainMnu);
        populateMenu(burgerMainMnu);
        populateMenu(m_mainMenuBar);

        m_humburgerAction->setVisible(false);
        m_mainMenuAction->setVisible(false);
        m_mainToolBar->setVisible(false);
    }
}

QMenu *Workspace::createFileMenu()
{
    auto *menu = new QMenu(tr("&File"));

    menu->setObjectName("fileMenu");

    if(m_documentNewAction)
        menu->addAction(m_documentNewAction);

    if(m_documentOpenAction)
    {
        if(!menu->isEmpty())
            menu->addSeparator();
        menu->addAction(m_documentOpenAction);
        menu->addAction(m_documentOpenRecentAction);
    }

    if(m_documentSaveAction)
    {
        if(!menu->isEmpty())
            menu->addSeparator();
        menu->addAction(m_documentSaveAction);
        menu->addAction(m_documentSaveAsAction);
        if(m_documentSaveAllAction)
            menu->addAction(m_documentSaveAllAction);
    }

    if(m_applicationQuitAction)
    {
        if(!menu->isEmpty())
            menu->addSeparator();
        menu->addAction(m_applicationQuitAction);
    }

    return menu;
}

QMenu *Workspace::createEditMenu()
{
    if(!afx::queryInterface<afx::IDocumentEditor>() &&
       !afx::queryInterface<afx::ISettingsEditor>())
        return nullptr;

    auto *menu = new QMenu(tr("&Edit"));
    connect(menu, &QMenu::aboutToShow, [this, menu](){
         auto addActions = [menu](const QList<QAction *> &actions) {
            bool insertSep = !menu->isEmpty();
            for(auto *a : actions)
                if(a)
                {
                    if(insertSep)
                    {
                        menu->addSeparator();
                        insertSep = false;
                    }
                    menu->addAction(a);
                }
        };

        menu->clear();
        if(auto *w = activeDocumentWindow())
            if(auto *editor = qobject_cast<afx::IDocumentEditor *>(w))
            {
                addActions({ editor->editUndoAction(),
                             editor->editRedoAction() });
                addActions({ editor->editCopyAction(),
                             editor->editCutAction(),
                             editor->editPasteAction(),
                             editor->editSelectAllAction() });
                addActions(editor->editAuxActions());
            }
        addActions({ m_settingsAction });
    });

    return menu;
}

QMenu *Workspace::createViewMenu()
{
    auto *menu = new QMenu(tr("&View"));
    return menu;
}

QMenu *Workspace::createWindowMenu()
{
    if(!m_mdiArea)
        return nullptr;

    auto *menu = new QMenu(tr("&Window"));
    connect(menu, &QMenu::aboutToShow, [this, menu](){
        menu->clear();
        menu->addAction(m_documentWindowDuplicate);
        menu->addSeparator();
        menu->addAction(m_documentWindowClose);
        menu->addAction(m_documentWindowCloseAll);
        menu->addSeparator();
        menu->addAction(m_documentWindowTile);
        menu->addAction(m_documentWindowCascade);
        menu->addAction(m_documentWindowViewMode);

        auto *activeSubWindow = m_mdiArea->activeSubWindow();
        m_documentWindowDuplicate->setEnabled(activeSubWindow);
        m_documentWindowClose->setEnabled(activeSubWindow);
        m_documentWindowCloseAll->setEnabled(activeSubWindow);
        m_documentWindowTile->setEnabled(activeSubWindow);
        m_documentWindowCascade->setEnabled(activeSubWindow);

        if(auto const &windows = m_mdiArea->subWindowList();!windows.empty())
        {
            menu->addSeparator();
            for(int i=0;i < windows.size();++i)
            {
                auto *mdiSubWindow = windows.at(i);
                auto const &text = QString("%1%2 %3").arg(i > 9 ? "" : "&").arg(i + 1).arg(mdiSubWindow->windowTitle());
                QAction *action = menu->addAction(text, mdiSubWindow, [this, mdiSubWindow]() {
                    m_mdiArea->setActiveSubWindow(mdiSubWindow);
                });
                action->setCheckable(true);
                action ->setChecked(mdiSubWindow == activeSubWindow);
            }
        }
    });
    return menu;
}

QMenu *Workspace::createHelpMenu()
{
    auto *menu = new QMenu(tr("&Help"));
    return menu;
}

void Workspace::createActions()
{
    m_documentNewAction = createNewDocumentAction();

    for(auto *p : m_plugins)
    {
        if(auto *serializer = qobject_cast<afx::IDocumentSerializer *>(p))
            registerDocumentSerializer(serializer);
        if(qobject_cast<afx::ISettingsEditor *>(p))
            m_settingsAction = createAction(ActionApplicationSettings).text(tr("Preferences...")).shortcut(QKeySequence::Preferences).get();
    }

    for(auto const &[ key, _ ] : m_documentFactories)
    {
        auto const &moniker = key.section(':', -2, -1);
        if(!m_documentOpenAction && m_documentReaders.contains(moniker))
        {
            m_documentOpenAction = createAction(ActionDocumentOpen).text(tr("&Open")).shortcut(QKeySequence::Open).get();
            m_documentOpenRecentAction = createAction(ActionDocumentOpenRecent).text(tr("Recent Files")).menu(new QMenu).get();
        }

        if(!m_documentSaveAction && m_documentWrirers.contains(moniker))
        {
            m_documentSaveAction = createAction(ActionDocumentSave).text(tr("&Save")).shortcut(QKeySequence::Save).get();
            m_documentSaveAsAction = createAction(ActionDocumentSaveAs).text(tr("Save &As...")).shortcut(QKeySequence::SaveAs).get();
            if(m_mdiArea)
            {
                m_documentSaveAllAction = createAction(ActionDocumentSaveAll).text(tr("Save A&ll")).get();
                m_documentSaveAllAction->setEnabled(false);
            }

            m_documentSaveAction->setEnabled(false);
            m_documentSaveAsAction->setEnabled(false);
        }
    }

    m_applicationQuitAction = createAction(ActionApplicationExit)
        .text(tr("&Quit")).shortcut(QKeySequence::Quit).connect(mainWindow(), SLOT(close()));

    if(m_mdiArea)
    {
        m_documentWindowClose = createAction(ActionWindowCloseActive).connect(m_mdiArea, SLOT(closeActiveSubWindow()))
            .text(tr("Cl&ose")).shortcut(QKeySequence::Close).statusTip(tr("Close the active window"));
        m_documentWindowCloseAll = createAction(ActionWindowCloseAll).connect(m_mdiArea, SLOT(closeAllSubWindows()))
            .text(tr("Close &All")).statusTip(tr("Close all the windows"));
        m_documentWindowDuplicate = createAction(ActionWindowDuplicate)
            .text(tr("New Window")).statusTip(tr("Open new window for the current document")).get();
        m_documentWindowTile = createAction(ActionWindowTile)
            .text(tr("&Tile")).statusTip(tr("Tile the windows")).get();
        m_documentWindowCascade = createAction(ActionWindowCascade)
            .text(tr("&Cascade")).statusTip(tr("Cascade the windows")).get();

        m_documentWindowViewMode = createAction(ActionWindowViewMode)
            .text("Tabed Mode").checkable().get();
    }
}

QAction *Workspace::createNewDocumentAction()
{
    for(auto *p : m_plugins)
        if(auto *factory = qobject_cast<afx::IDocumentFactory *>(p))
            registerDocumentFactory(factory);

    if(!m_documentFactories.empty())
    {
        auto *action = createAction(ActionDocumentNew).text(tr("&New")).shortcut(QKeySequence::New).get();
        for(auto const &[key, _] : m_documentFactories)
        {
            // name : MIME
            QString const &name = key.section(':', -2, -2);
            QString const &mime = key.section(':', -1, -1);
            QAction *a = action;
            if(m_documentFactories.size() > 1)
            {
                auto const &objName = QString("%1-%2").arg(ActionDocumentNew, mime).toLatin1();
                a = createAction(objName.data()).text(name).connect(this, SLOT(onDocumentNew()));
                if(!action->menu())
                {
                    action->setMenu(new QMenu);
                    action->setShortcut({});
                }
                action->menu()->addAction(a);
            }

            a->setData(key);
            a->setEnabled(true);
        }

        return action;
    }

    return nullptr;
}

void Workspace::updateEditActions(afx::IDocumentEditor *editor)
{
    for(auto &[_, p] : m_editActionButtons)
    {
        auto [button, action] = p;
        button->setEnabled(false);
        button->disconnect();
        if(action)
            action->disconnect(button);

        p.second = {};
    }

    if(editor)
    {
        std::pair<std::string_view, QAction *> const pairs[] = {
            { ActionEditUndo,  editor->editUndoAction() },
            { ActionEditRedo,  editor->editRedoAction() },
            { ActionEditCopy,  editor->editCopyAction() },
            { ActionEditCut,   editor->editCutAction() },
            { ActionEditPaste, editor->editPasteAction() } };
        for(auto const &[name, action] : pairs)
        {
            if(action)
            {
                auto &[button, _] = m_editActionButtons[name];

                button->setEnabled(action->isEnabled());
                connect(button, &QToolButton::clicked, action, &QAction::trigger);
                connect(action, &QAction::enabledChanged, button, &QToolButton::setEnabled);
            }

            m_editActionButtons[name].second = action;
        }
    }
}

QObject *Workspace::activeDocument() const
{
    if(auto *editor = qobject_cast<afx::IDocumentEditor *>(activeDocumentWindow()))
        return editor->document();
    return {};
}

QWidget *Workspace::activeDocumentWindow() const
{
    if(m_mdiArea)
        if(auto *subWin = m_mdiArea->activeSubWindow())
            return subWin->widget();
    return {};
}

void Workspace::onApplicationStateChanged(Qt::ApplicationState state)
{
    qDebug() << "Workspace::onApplicationStateChanged" << state;
}

void Workspace::onPreferences()
{
    if(auto *se = afx::queryInterface<afx::ISettingsEditor>())
    {
        se->showSettingEditor(mainWindow());
    }
}

void Workspace::onWindowViewMode()
{
    if(m_documentWindowViewMode->isChecked())
    {
        m_mdiArea->setViewMode(QMdiArea::TabbedView);
        m_mdiArea->setTabPosition(QTabWidget::South);
        m_mdiArea->setTabShape(QTabWidget::Triangular);
        m_mdiArea->setDocumentMode(true);
    }
    else
        m_mdiArea->setViewMode(QMdiArea::SubWindowView);
}

void Workspace::onWindowTile()
{
    m_documentWindowViewMode->setChecked(false);
    m_mdiArea->tileSubWindows();
    onWindowViewMode();
}

void Workspace::onWindowCascade()
{
    m_documentWindowViewMode->setChecked(false);
    m_mdiArea->cascadeSubWindows();
    onWindowViewMode();
}

void Workspace::onWindowDuplicate()
{
    if(m_mdiArea)
    {
        if(auto *subWindow = m_mdiArea->currentSubWindow())
            if(auto *editor = qobject_cast<afx::IDocumentEditor *>(subWindow->widget()))
            {
                auto *doc = editor->document();

                Q_ASSERT(m_documents.contains(doc));
                auto const &moniker = m_documents[doc].moniker;

                Q_ASSERT(m_documentFactories.contains(moniker));
                auto *factory = m_documentFactories[moniker];

                if(auto *widget = factory->createEditor(moniker, mainWindow()))
                {
                    if(setupDocumentWindow(moniker, doc, widget))
                        return;
                    delete widget;
                }
            }
    }
}

void Workspace::onSubWindowActivated(QMdiSubWindow *subWindow)
{
    afx::IDocumentEditor *editor = {};

    if(subWindow)
        editor = qobject_cast<afx::IDocumentEditor *>(subWindow->widget());

    updateEditActions(editor);
}
