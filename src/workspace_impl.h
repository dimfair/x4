#ifndef WORKSPACE_IMPL_H_202309031922
#define WORKSPACE_IMPL_H_202309031922

#include "workspace.h"

#include "connection.h"

#include <unordered_map>
#include <unordered_set>

namespace afx {
template<typename T>
class InrterfaceRegisrty;

class IDocument;
class IDocumentEditor;
class IDocumentFactory;
class IDocumentSerializer;
}

class QCloseEvent;
class QGuiApplication;
class QMainWindow;
class QMdiArea;
class QMdiSubWindow;
class QMenuBar;
class QToolBar;
class QToolButton;

class Workspace : public QObject, public afx::IWorkspace
{
    Q_OBJECT
    Q_INTERFACES( afx::IWorkspace )

public:
    Workspace(bool isMdi, QGuiApplication *);
   ~Workspace();

    void loadPlugins(const QStringList &paths = {}) override;
    QObjectList plugins() const override;

    void registerDocumentFactory(afx::IDocumentFactory *) override;
    void registerDocumentSerializer(afx::IDocumentSerializer *) override;
    std::map<QString, QAction *> actions() const override;
    afx::ActionBuilder createAction(const char *name, QObject *parent = {}) override;
    QIcon loadIcon(const char *name) override;

    virtual afx::MainWindowLayout mainWindowLayout() const override;
    virtual void setMainWindowLayout(afx::MainWindowLayout) override;

    void show(afx::MainWindowLayout) override;

private slots:
    void onApplicationStateChanged(Qt::ApplicationState);
    void onPreferences();

    void onDocumentNew();
    void onDocumentOpen();
    void onDocumentOpenRecent();
    void onDocumentSave();
    void onDocumentSaveAs();
    void onDocumentSaveAll();
    void onDocumentCleanStateChanged();
    void onDocumentDestroy();

    void onWindowViewMode();
    void onWindowTile();
    void onWindowCascade();
    void onWindowDuplicate();
    void onSubWindowActivated(QMdiSubWindow *);

private:
    static const QString s_keyMainWindow;
    static const QString s_keyLastOpenFileDir;
    static QString normalizeMoniker(const QString &);

    bool eventFilter(QObject *, QEvent *) override;

    QMainWindow *mainWindow();
    QSettings *settings();

    bool processMainWndEvent(QEvent *);
    bool processMdiSubwindowEvent(QEvent *, QMdiSubWindow *);
    bool processDocumentCloseEvent(QCloseEvent *, QObject *);
    void saveMainWindowState(QSettings *) const;
    void restoreMainWindowState(QSettings *);
    void initializeMainWnd();
    void createMenus();
    void createActions();

    QMenu *createFileMenu();
    QMenu *createEditMenu();
    QMenu *createViewMenu();
    QMenu *createWindowMenu();
    QMenu *createHelpMenu();
    QAction *createNewDocumentAction();
    void updateEditActions(afx::IDocumentEditor *);

    QObject *activeDocument() const;
    QWidget *activeDocumentWindow() const;

    bool saveDocument(QObject *, bool saveAs = false);
    bool setupDocumentWindow(const QString &, QObject *, QWidget *);

    bool m_isMainWndInitialized = false;
    afx::MainWindowLayout m_mainWindowLayout = afx::MainWindowLayout::Classic;

    QMainWindow *m_mainWnd{};
    QMenuBar *m_mainMenuBar{};
    QToolBar *m_mainToolBar{};
    QToolButton *m_burgerButton{};
    QSettings *m_settings{};
    std::unordered_map<QString, afx::IDocumentFactory *> m_documentFactories;
    std::unordered_map<QString, afx::IDocumentSerializer *> m_documentReaders;
    std::unordered_map<QString, afx::IDocumentSerializer *> m_documentWrirers;

    QObjectList m_plugins;

    QAction *m_humburgerAction{};
    QAction *m_mainMenuAction{};

    QAction *m_applicationQuitAction{};
    QAction *m_documentNewAction{};
    QAction *m_documentOpenAction{};
    QAction *m_documentOpenRecentAction{};
    QAction *m_documentSaveAction{};
    QAction *m_documentSaveAsAction{};
    QAction *m_documentSaveAllAction{};

    QAction *m_settingsAction{};

    QAction *m_documentWindowClose{};
    QAction *m_documentWindowCloseAll{};
    QAction *m_documentWindowDuplicate{};
    QAction *m_documentWindowTile{};
    QAction *m_documentWindowCascade{};
    QAction *m_documentWindowViewMode{};

    std::map<std::string_view, std::pair<QToolButton *, QAction *>> m_editActionButtons;

    QMenu *m_fileMenu{};
    QMenu *m_editMenu{};
    QMenu *m_viewMenu{};
    QMenu *m_windowMenu{};
    QMenu *m_helpMenu{};

    QMdiArea *m_mdiArea{};
    std::unordered_map<QMdiSubWindow *, QObject *> m_mdiWigdetMap;

    struct DocInfo {
        QString moniker;
        QString url;
        afx::IDocument *document;
        std::unordered_set<QWidget *> editors;

        afx::Connection connCleanChange;
    };
    std::unordered_map<QObject *, DocInfo> m_documents;

    std::unordered_map<QString, QString> m_lastSaveDirs;
};

#endif // WORKSPACE_IMPL_H_202309031922
