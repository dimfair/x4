#pragma once

#include <QAction>
#include <QCoreApplication>
#include <QKeySequence>
#include <QMenu>

namespace afx {

class ActionBuilder
{
public:
    ActionBuilder() = delete;
    ActionBuilder(const ActionBuilder &) = delete;
    ActionBuilder(ActionBuilder &&) = delete;
    ActionBuilder & operator =(const ActionBuilder &) = delete;

    ActionBuilder(QAction *a) : action(a) {}

    QAction *get() const                        { return action; }
    operator QAction *() const                  { return get(); }

    ActionBuilder &enable(bool f = true)                { action->setEnabled(f); return *this; }
    ActionBuilder &disable()                            { action->setEnabled(false); return *this; }
    ActionBuilder &shortcut(const QKeySequence &ks)     { action->setShortcut(ks); return *this; }
    ActionBuilder &shortcut(const QString &ks)          { action->setShortcut(ks); return *this; }
    ActionBuilder &shortcut(QKeySequence::StandardKey k){ action->setShortcut(k); return *this; }
    ActionBuilder &statusTip(const QString &text)       { action->setStatusTip(text); return *this; }
    ActionBuilder &toolTip(const QString &text)         { action->setToolTip(text); return *this; }
    ActionBuilder &text(const QString &str)             { action->setText(str); return *this; }
    ActionBuilder &icon(const QIcon &icon)              { action->setIcon(icon); return *this; }
    ActionBuilder &parent(QObject *obj)                 { action->setParent(obj); return *this; }
    ActionBuilder &checkable(bool f = true)             { action->setCheckable(f); return *this; }
    ActionBuilder &menu(QMenu *m)                       { action->setMenu(m); return *this; }
    ActionBuilder &data(const QVariant &v)              { action->setData(v); return *this; }

    ActionBuilder &connect(QObject *receiver, const char *slot) {
        auto conn = QObject::connect(action, SIGNAL(triggered()), receiver, slot);
        action->setEnabled(conn);
        return *this;
    }

    template<typename Functor>
    ActionBuilder &connect(QObject *receiver, Functor &&func) {
        auto conn = QObject::connect(action, &QAction::triggered, receiver, func);
        action->setEnabled(conn);
        return *this;
    }

    QAction *action;
};

}
