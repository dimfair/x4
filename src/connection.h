#ifndef AFX_CONNECTION_H__
#define AFX_CONNECTION_H__

#include <functional>
#include <memory>

namespace afx {
namespace details {

struct ConnectionHandle : public std::enable_shared_from_this<ConnectionHandle>
{
};

template <typename T>
struct ConnectionImpl;

template <typename R, typename ...Args>
struct ConnectionImpl<std::function<R(Args...)>> : public ConnectionHandle
{
    using Callback = std::function<R(Args...)>;
    using SelfT = ConnectionImpl<Callback>;

    static std::shared_ptr<SelfT> create(const Callback &cb) {
        struct builder : SelfT { builder(const Callback &cb) : SelfT(cb) {} };
        return std::make_shared<builder>(cb);
    }

    void notify(Args... args) const { cb_(args...); }

protected:
    ConnectionImpl() = delete;
    ConnectionImpl(const Callback &cb) : cb_(cb) {}
    Callback cb_;
};

}   // namespace details

template <typename R>
using Response =   std::weak_ptr<details::ConnectionImpl<R>>;
using Connection = std::shared_ptr<details::ConnectionHandle>;

// Returns connection object made from response functor
template <typename T>
std::shared_ptr<details::ConnectionImpl<T>> make_connection(const T &resp) {
    return details::ConnectionImpl<T>::create(resp);
}

// Returns true if response is not expired or
// false in other case
template <typename Resp, typename ...Args>
bool call_response(const Resp &r, Args... args) {
    if(auto x = r.lock())
    {
        x->notify(args...);
        return true;
    }
    return false;
}

}   // namespace afx

#endif // CONNECTION_H__
