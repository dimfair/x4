#ifndef WORKSPACE_H_202309031359
#define WORKSPACE_H_202309031359

#include <QCoreApplication>

constexpr const char *ActionApplicationExit                 = "application-exit";
constexpr const char *ActionApplicationSettings             = "preferences";

constexpr const char *ActionDocumentNew                     = "document-new";
constexpr const char *ActionDocumentOpen                    = "document-open";
constexpr const char *ActionDocumentOpenRecent              = "document-open-recent";
constexpr const char *ActionDocumentSave                    = "document-save";
constexpr const char *ActionDocumentSaveAs                  = "document-save-as";
constexpr const char *ActionDocumentSaveAll                 = "document-save-all";
constexpr const char *ActionDocumenPageSetup                = "document-page-setup";
constexpr const char *ActionDocumentPrint                   = "document-print";
constexpr const char *ActionDocumentPrintPreview            = "document-print-preview";

constexpr const char *ActionEditClear                       = "edit-clear";
constexpr const char *ActionEditSelectAll                   = "edit-select-all";
constexpr const char *ActionEditCopy                        = "edit-copy";
constexpr const char *ActionEditCut                         = "edit-cut";
constexpr const char *ActionEditPaste                       = "edit-paste";
constexpr const char *ActionEditRedo                        = "edit-redo";
constexpr const char *ActionEditUndo                        = "edit-undo";
constexpr const char *ActionEditFind                        = "edit-find";
constexpr const char *ActionEditFindReplace                 = "edit-find-replace";

constexpr const char *ActionViewMainMenu                    = "veiw-main-menu";

constexpr const char *ActionWindowCloseActive               = "window-close-active";
constexpr const char *ActionWindowCloseAll                  = "window-close-all";
constexpr const char *ActionWindowDuplicate                 = "window-duplicate";
constexpr const char *ActionWindowTile                      = "window-tile";
constexpr const char *ActionWindowCascade                   = "window-cascade";
constexpr const char *ActionWindowViewMode                  = "window-view-mode";

constexpr const char *ActionHelpAbout                       = "help-about";
constexpr const char *ActionHelpContents                    = "help-contents";

constexpr const char *ActionSeparator                       = nullptr;

///////////////////////////////////////////////////////////////////////////////////////////

constexpr const char *RegKeyWindowState                     = "MainWindow/State";
constexpr const char *RegKeyWindowSize                      = "MainWindow/Size";
constexpr const char *RegKeyWindowPos                       = "MainWindow/Pos";
constexpr const char *RegKeyWindowLayout                    = "MainWindow/Layout";
constexpr const char *RegKeyCompactMenu                     = "MainWindow/CompactMenu";


class QAbstractItemDelegate;
class QAction;
class QGuiApplication;
class QIcon;
class QMenu;
class QSettings;
class QToolBar;
class QWidget;

namespace afx {

enum class MainWindowLayout {
    Classic,
    Compact,
    HamburgerMenu
};

class IDocumentFactory;
class IDocumentSerializer;
class ActionBuilder;

class IWorkspace
{
public:
    virtual ~IWorkspace() = default;

    virtual void loadPlugins(const QStringList &paths = {}) = 0;
    virtual QObjectList plugins() const = 0;

    // returns list of joined (with ':') factoryId and normalized monikers
    virtual void registerDocumentFactory(IDocumentFactory *) = 0;
    virtual void registerDocumentSerializer(IDocumentSerializer *) = 0;

    virtual std::map<QString, QAction *> actions() const = 0;
    virtual ActionBuilder createAction(const char *name, QObject * = {}) = 0;

    virtual QIcon loadIcon(const char *name) = 0;

    virtual MainWindowLayout mainWindowLayout() const = 0;
    virtual void setMainWindowLayout(MainWindowLayout) = 0;

    virtual void show(MainWindowLayout = MainWindowLayout::Compact) = 0;
};



class IPlugin
{
public:
    virtual ~IPlugin() = default;

    virtual QString name() const = 0;
    virtual QString description() const = 0;

    // called when all other plugins has been loaded
    // but not initialized.
    virtual bool initialize() = 0;

    // called when all plugins are loaded and inited
    virtual void loadState(QSettings *) = 0;

    // called before application exit
    virtual void saveState(QSettings *) const = 0;
};


class IUiBuilder
{
public:
    virtual ~IUiBuilder() = default;

    virtual QList<QMenu *> menus() const = 0;
    virtual QList<QToolBar *> toolBars() const = 0;
    virtual QList<QWidget *> statusBarWidgets() const = 0;
    virtual QList<QWidget *> dockWidgets() const = 0;
};

class ILogger
{
public:
    virtual ~ILogger() = default;

    virtual void log(QtMsgType, const QMessageLogContext &, const QString &) = 0;
};

class ISettingsEditor
{
public:
    using WidgetFactory = std::function<QWidget *(QWidget *)>;

    virtual ~ISettingsEditor() = default;

    virtual void registerValueListForm(const QString &name, const QString &key, QAbstractItemDelegate * = nullptr) = 0;

    virtual void registerSettingsForm(const WidgetFactory &ctor, const QString &name, const QStringList &props, const QString &key) = 0;
    virtual int showSettingEditor(QWidget *parent) = 0;

    template<typename T>
    void registerSettingsForm(const QString &name, const QStringList &props, const QString &key) {
        registerSettingsForm([](QWidget *p){ return new T(p); }, name, props, key);
    }
};

class IPluginMgr
{
public:
    virtual ~IPluginMgr() = default;

    virtual QStringList pluginPaths() = 0;
};

IWorkspace *createMdiWorkspace(QGuiApplication *);

}   // namespace afx

Q_DECLARE_INTERFACE( afx::IPlugin,           "IPlugin/1.0" );
Q_DECLARE_INTERFACE( afx::IPluginMgr,        "IPluginMgr/1.0" );
Q_DECLARE_INTERFACE( afx::ILogger,           "ILogger/1.0" );
Q_DECLARE_INTERFACE( afx::IUiBuilder,        "IUIBuilder/1.0" );
Q_DECLARE_INTERFACE( afx::ISettingsEditor,   "ISettingsEditor/1.0" );
Q_DECLARE_INTERFACE( afx::IWorkspace,        "IWorkspace/2.0" );

namespace afx {

inline IWorkspace *workspace() {
    if(auto *ws = QCoreApplication::instance()->findChild<QObject *>("AfxWorkspace", Qt::FindDirectChildrenOnly))
        return qobject_cast<IWorkspace *>(ws);
    return nullptr;
}

template<typename T>
inline std::vector<T*> queryInterfaces() {
    std::vector<T*> list;
    for(auto const &pp = workspace()->plugins(); auto *p : pp)
        if(auto *iface = qobject_cast<T *>(p))
            list.push_back(iface);
    return list;
}

template<typename T>
inline T* queryInterface() {
    for(auto const &pp = workspace()->plugins(); auto *p : pp)
        if(auto *iface = qobject_cast<T *>(p))
            return iface;
    return {};
}

}

#endif // WORKSPACE_H_202309031359
