#ifndef AFX_DOCUMENT_H_202309031331
#define AFX_DOCUMENT_H_202309031331

#include <QObject>

#include <afx/Connection>

class QAction;

namespace afx {

class IDocument
{
public:
    using CleanChengeNotify = std::function<void(bool)>;

    virtual ~IDocument() = default;

    virtual QString name() const = 0;
    virtual QString mime() const = 0;

    virtual bool isClean() const = 0;
    virtual void setClean(bool = true) = 0;
    virtual Connection cleanChanged(CleanChengeNotify &&) = 0;
};

class IDocumentEditor
{
public:
    using CopyAvailableNotify = std::function<void(bool)>;
    using UndoAvailableNotify = std::function<void(bool, QString)>;

    virtual ~IDocumentEditor() = default;

    virtual void initialize(QObject *) = 0;

    virtual QObject *document() const = 0;

    // Creates and eturns undo action. The actions is owned by the editor.
    // Returns nullptr if the editor does not support undo/redo functionality.
    virtual QAction *editUndoAction() = 0;
    // Creates and eturns redo action. The actions is owned by the editor.
    // Returns nullptr if the editor does not support undo/redo functionality.
    virtual QAction *editRedoAction() = 0;

    virtual QAction *editClearAction() = 0;
    virtual QAction *editCopyAction() = 0;
    virtual QAction *editCutAction() = 0;
    virtual QAction *editPasteAction() = 0;
    virtual QAction *editSelectAllAction() = 0;

    // Creates and eturns auxilary edit actions (or empty list if there are no such actions). The actions is owned by
    // the editor.
    virtual QList<QAction *> editAuxActions() = 0;
};

class IDocumentFactory
{
public:
    virtual ~IDocumentFactory() = default;

    // returns string should contain a unique value which is
    // plugin's identificator.
    // It could be a GUID or some other unique string identifier.
    virtual QString factoryId() const = 0;

    // Document factory can create one or more document types.
    // Each document type is described by specially formatted string
    // which is called "moniker".
    // Moniker is a token list delimited by ':'.
    // The 1st token is a short document descriptor, the 2nd one is a MIME type,
    // other tokens is not used currently.
    // Examplples:
    //  'Text Document : text/*'
    //  'Spreadsheet : application/x-table'
    virtual QStringList documentMonikers() const = 0;

    // Creates a document object using moniker as a document type selector.
    // Returns null if this factrory cannot create document with a given moniker
    virtual QObject *createDocument(const QString &moniker = {}) const = 0;

    // Creates a document editor (QWidget derived object) using moniker as a document
    // type selector and a given parent.
    // Returns null if this factrory cannot create editor with a given moniker
    virtual QWidget *createEditor(const QString &moniker, QWidget *parent) const = 0;
};

class IDocumentSerializer
{
public:
    virtual ~IDocumentSerializer() = default;

    // Maps document moniker to file filter
    // Filter is a string with a file description (see QFileDialog for
    // the further info)
    virtual QString filterText(const QString &moniker) const = 0;

    // Returns true if the serializer is able to deserialize document with
    // the given moniker
    virtual bool canRead(const QString &moniker) const = 0;

    // Returns true if the serializer is able to serialize document with
    // the given moniker
    virtual bool canWrite(const QString &moniker) const = 0;

    // Initializes the document with a given moniker by data from the device
    virtual int read (QObject *doc, QIODevice *device, const QString &moniker) const = 0;

    // Serializes the document with a given moniker to the device
    // Retrurns error code or 0 on success.
    virtual int write(QObject *doc, QIODevice *device, const QString &moniker) const = 0;
};

}   // namespace afx


Q_DECLARE_INTERFACE(afx::IDocument, "IDocument/2.0")
Q_DECLARE_INTERFACE(afx::IDocumentFactory, "IDocumentFactory/2.0")
Q_DECLARE_INTERFACE(afx::IDocumentSerializer, "IDocumentSerializer/2.0");
Q_DECLARE_INTERFACE(afx::IDocumentEditor, "IDocumentEditor/2.0");

#endif // AFX_DOCUMENT_H_202309031331

