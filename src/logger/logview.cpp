#include "logview.h"
#include "ui_logview.h"

#include <QAbstractListModel>

#include <array>
#include <chrono>
#include <deque>
#include <string>
#include <unordered_set>

namespace afx { namespace plugins {
namespace details {

static inline
uint64_t now() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

class LogModel : public QAbstractListModel
{
public:
    LogModel(QObject *parent = nullptr)
        : QAbstractListModel(parent)
    {
    }

    int columnCount(const QModelIndex & = {}) const override {
        return static_cast<qsizetype>(m_headers.size());
    }

    int rowCount(const QModelIndex &parent = {}) const override {
        if(!parent.isValid())
            return static_cast<qsizetype>(m_log.size());
        return 0;
    }

    QVariant data(const QModelIndex &, int = Qt::DisplayRole) const override {
        return {};
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override {
        Q_ASSERT(section >=0 && section < columnCount());
        if(orientation == Qt::Horizontal && role == Qt::DisplayRole)
            return m_headers[section];
        return {};
    }

private:
    static inline const std::array m_headers{ tr("Type"), tr("Timestamp"), tr("Function"), tr("Message") };

    struct Msg {
        uint64_t    timestump;
        QtMsgType   type;
        QString     msg;
        const char *func;
    };

    std::unordered_set<std::string> m_functions;
    std::deque<Msg> m_log;
};

LogView::LogView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogView),
    m_startTime(now()),
    m_model(new LogModel(this))
{
    ui->setupUi(this);
}

LogView::~LogView()
{
    delete ui;
}

void LogView::log(QtMsgType mType, const QString &line)
{
    auto time = now() - m_startTime;
    uint64_t secs  = time/1000;
    uint64_t msecs = time%1000;

    QString type;
    switch(mType)
    {
    case QtDebugMsg:    type = "D"; break;
    case QtWarningMsg:  type = "W"; break;
    case QtCriticalMsg: type = "E"; break;
    case QtFatalMsg:    type = "F"; break;
    case QtInfoMsg:     type = "I"; break;
    }

    ui->plainTextEdit->appendPlainText(QString("%1 | %2.%3 | %4").arg(type).arg(secs, 4).arg(msecs, 3, 10, QChar('0')).arg(line));
}

void LogView::log(QtMsgType mType, const std::string &func, const QString &line)
{
    Q_UNUSED(mType)
    Q_UNUSED(func)
    Q_UNUSED(line)
//    auto const &[it, _] = m_functions.insert(func);
//    m_log.emplace_back(
//        now() - m_startTime,
//        mType,
//        line,
//        it->c_str()
//    );
}

}
}}
