#ifndef LOGVIEW_H
#define LOGVIEW_H

#include <QDebug>
#include <QWidget>


namespace Ui {
    class LogView;
}

namespace afx { namespace plugins {
namespace details {

class LogModel;

class LogView : public QWidget
{
    Q_OBJECT

public:
    explicit LogView(QWidget *parent = nullptr);
   ~LogView();

    void log(QtMsgType, const QString &line);
    void log(QtMsgType, const std::string &func, const QString &line);

private:
    Ui::LogView *ui;
    uint64_t m_startTime;
    LogModel *m_model;
};

}}
}

#endif // LOGVIEW_H
