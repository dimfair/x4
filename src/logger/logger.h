#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>

#include "../workspace.h"

namespace afx {
namespace plugins {

namespace details {
    class LogView;
}

class Logger : public QObject,
               public afx::IPlugin,
               public afx::IUiBuilder,
               public afx::ILogger
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.xvier-project.Plugins.Logger" FILE "Logger.json")
    Q_INTERFACES( afx::IPlugin afx::IUiBuilder afx::ILogger )

public:
    Logger(QObject *parent = nullptr);
   ~Logger() override;

    // afx::ILogger
    void log(QtMsgType, const QMessageLogContext &, const QString &) override;

    // afx::IPlugin
    QString name() const override;
    QString description() const override;
    bool initialize() override;
    void loadState(QSettings *) override;
    void saveState(QSettings *) const override;

    // afx::IUIBuilder
    QList<QMenu *> menus() const override;
    QList<QToolBar *> toolBars() const override;
    QList<QWidget *> statusBarWidgets() const override;
    QList<QWidget *> dockWidgets() const override;

private:
    details::LogView *m_logView;
};

}   // ns plugins
}   // ns afx

#endif // LOGGER_H
