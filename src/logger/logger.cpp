#include "logger.h"
#include "logview.h"

#include <QDebug>

namespace afx {
namespace plugins {

static QtMessageHandler DefHandler = nullptr;
static Logger *instance = nullptr;

//static
//const char *cutFunc(const char *str)
//{
//    auto *pp = str;
//    for(auto *p=pp;*p;++p)
//        if(*p == ' ')
//            pp = p;

//    return ++pp;
//}

static
void MsgHandler(QtMsgType mtype, const QMessageLogContext &ctx, const QString &msg)
{
    Q_UNUSED(ctx)
    //instance->logString(mtype, QString("[%1] %2").arg(cutFunc(ctx.function), msg));
    instance->log(mtype, ctx, msg);
    DefHandler(mtype, ctx, msg);
}


Logger::Logger(QObject *parent)
    : QObject(parent),
      m_logView(new details::LogView)
{
    setObjectName("AfxLoggerPlugin");
    instance = this;
}

Logger::~Logger()
{
    qInstallMessageHandler(DefHandler);
}

void Logger::log(QtMsgType type, const QMessageLogContext &, const QString &line)
{
    m_logView->log(type, line);
}

QString Logger::name() const
{
    return "Logger";
}

QString Logger::description() const
{
    return "Log controller";
}

bool Logger::initialize()
{
    DefHandler = qInstallMessageHandler(MsgHandler);
    return true;
}

void Logger::loadState(QSettings *)
{
}

void Logger::saveState(QSettings *) const
{
}

QList<QMenu *> Logger::menus() const
{
    return {};
}

QList<QToolBar *> Logger::toolBars() const
{
    return {};
}

QList<QWidget *> Logger::statusBarWidgets() const
{
    return {};
}

QList<QWidget *> Logger::dockWidgets() const
{
    return { m_logView };
}

}   // ns plugins
}   // ns afx
