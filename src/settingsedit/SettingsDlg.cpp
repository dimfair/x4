#include "SettingsDlg.h"
#include "settingseditor.h"
#include "itemlistform.h"

#include <QMenu>
#include <QSettings>

namespace afx {
namespace plugins {

struct SettingsEditor::SettingsFormInfo
{
    using WidgetFactory = afx::ISettingsEditor::WidgetFactory;

    SettingsFormInfo() = delete;
    SettingsFormInfo(const SettingsFormInfo &) = delete;

    SettingsFormInfo(
        const WidgetFactory &f,
        const QStringList &props,
        const QString &k) : ctor(f), key(k), properties(props) {}

   ~SettingsFormInfo() = default;

    QWidget *createForm(QWidget *parent = nullptr) const {
        return qobject_cast<QWidget *>(ctor(parent));
    }

    WidgetFactory   ctor;
    QString         key;
    QStringList     properties;
};

SettingsEditor::SettingsEditor(QObject *parent)
    : QObject(parent)
{
    setObjectName("AfxSettingsDlgPlugin");
}

SettingsEditor::~SettingsEditor()
{
    for(auto &[_, info] : m_settingsForms)
        delete info;
}

QString SettingsEditor::name() const
{
    return "SettingsDlg";
}

QString SettingsEditor::description() const
{
    return "SettingsDlg controller";
}

bool SettingsEditor::initialize()
{
    return true;
}

void SettingsEditor::loadState(QSettings *)
{
}

void SettingsEditor::saveState(QSettings *) const
{
}

void SettingsEditor::registerValueListForm(const QString &name, const QString &key, QAbstractItemDelegate *deleg)
{
    auto factory = [deleg](QWidget *parent) -> QWidget * {
        auto *w = new ItemListForm(parent);
        w->setDelegate(deleg);
        w->setLabelText({});
        return w;
    };

    registerSettingsForm(factory, name, { "values" }, key);
}

void SettingsEditor::registerSettingsForm(const WidgetFactory &ctor, const QString &name, const QStringList &props, const QString &key)
{
    m_settingsForms[name] = new SettingsFormInfo(ctor, props, key);
}

int SettingsEditor::showSettingEditor(QWidget *parent)
{
    if(auto *settings = afx::queryInterface<QSettings>())
    {
        AfxSettingsDialog dlg(parent);

        for(auto const &[ key, formInfo ] : m_settingsForms)
        {
            if(auto *w = formInfo->createForm())
            {
                auto regKey = key;
                if(!formInfo->key.isEmpty())
                    regKey = formInfo->key;

                for(auto const &prop : formInfo->properties)
                    w->setProperty(prop.toLatin1(), settings->value(regKey + "/" + prop));

                dlg.addPage(w, key);
            }
        }

        if(dlg.exec() == QDialog::Accepted)
        {
            for(auto const &[ key, formInfo ] : m_settingsForms)
            {
                if(auto *w = dlg.page(key))
                {
                    auto regKey = key;
                    if(!formInfo->key.isEmpty())
                        regKey = formInfo->key;

                    for(auto const &prop : formInfo->properties)
                        settings->setValue(regKey + "/" + prop, w->property(prop.toLatin1()));
                }
            }

            return QDialog::Accepted;
        }
    }

    return QDialog::Rejected;
}

}   // plugins
}   // afx
