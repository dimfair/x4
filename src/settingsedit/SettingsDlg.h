#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>

#include "../workspace.h"

namespace afx {
namespace plugins {

class SettingsEditor : public QObject,
                    public afx::IPlugin,
                    public afx::ISettingsEditor
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.xvier-project.Plugins.SettingsEditor" FILE "SettingsEditor.json")
    Q_INTERFACES( afx::IPlugin afx::ISettingsEditor )

public:
    SettingsEditor(QObject *parent = nullptr);
   ~SettingsEditor() override;

    // afx::IPlugin overrides
    QString name() const override;
    QString description() const override;
    bool initialize() override;
    void loadState(QSettings *) override;
    void saveState(QSettings *) const override;

    // afx::ISettingsEditor overrides
    void registerValueListForm(const QString &name, const QString &key, QAbstractItemDelegate * = nullptr) override;
    void registerSettingsForm(const WidgetFactory &ctor, const QString &name, const QStringList &props, const QString &key) override;
    int showSettingEditor(QWidget *parent) override;

private:
    struct SettingsFormInfo;
    std::unordered_map<QString, SettingsFormInfo *> m_settingsForms;
};

}   // plugins
}   // afx

#endif // LOGGER_H
