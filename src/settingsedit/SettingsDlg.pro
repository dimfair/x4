include (../plugins.pri)

QT += widgets
CONFIG += staticlib

SOURCES += \
    SettingsDlg.cpp \
    form.cpp \
    itemlistform.cpp \
    settingseditor.cpp

HEADERS += \
    SettingsDlg.h \
    form.h \
    itemlistform.h \
    settingseditor.h

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    SettingsDlg.json

FORMS += \
    itemlistform.ui \
    settingseditor.ui
