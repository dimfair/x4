#ifndef __QFX_CONFIGFORM_H
#define __QFX_CONFIGFORM_H

#include <QVariantMap>
#include <QWidget>

class AfxForm : public QWidget
{
    Q_OBJECT

protected:
    QVariantMap m_dict;

    bool isPropertyDeclared(const QString &) const;

public:
    AfxForm(QWidget *parent = nullptr) : QWidget(parent) {}
   ~AfxForm() = default;

    QVariantMap values() const;
    QVariant value(const QString &, bool lookUp = true) const;
    void setValue(const QString &, const QVariant &);

    virtual void reset () { }
    virtual void commit() { }
    virtual bool canCommit()     const { return true; }
    virtual bool isAppropriate() const { return true; }

    int exec();

signals:
    void stateChanged();
};

#endif  // __QFX_CONFIGFORM_H
