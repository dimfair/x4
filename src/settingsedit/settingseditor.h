#ifndef __QFX_SETTINGSDLG_H
#define __QFX_SETTINGSDLG_H

#include <QMap>
#include "ui_settingseditor.h"

class AfxSettingsDialog : public QDialog
{
    Q_OBJECT

private:
    Ui::SettingsDialog ui;
    QMap<QString, int> m_pageIndices;
    QTreeWidgetItem *m_prevItem;

    QTreeWidgetItem *createTreeItem(const QStringList &, QTreeWidgetItem * = 0);

private slots:
    void onTreeItemChanged(QTreeWidgetItem *, QTreeWidgetItem*);
    void selectPage(const QString &);

public:
    AfxSettingsDialog(QWidget *);

    int addPage(QWidget *, const QString & = QString());

	int count() const;
	QWidget *page(int) const;
	QWidget *page(const QString &) const;

    void accept();
};

#endif  // __QFX_SETTINGSDLG_H
