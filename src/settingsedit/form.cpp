
#include <QDebug>

#include <QDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QMetaProperty>
#include <QPushButton>
#include <QSpacerItem>

#include "form.h"

class ModalFrame : public QDialog
{
    private:
        AfxForm *m_form;
        QWidget *m_formParent;
        QGridLayout *grid;

    public:
        ModalFrame(AfxForm *);
       ~ModalFrame();

       void accept();
       void done(int);
};

ModalFrame::ModalFrame(AfxForm *form)
    : QDialog(form ? form->parentWidget() : 0 )
{
    m_form = form;
    if(m_form)
    {
        m_formParent = form->parentWidget();
        m_form->setParent(0);
        setWindowTitle(m_form->windowTitle());

        //connect(m_form, SIGNAL(stateChanged()), this, SLOT(stateChanged()));
    }

    grid = new QGridLayout(this);
    grid->setSpacing(6);
    //grid->setMargin(9);
    grid->setObjectName(QString::fromUtf8("gridLayout"));

    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->setObjectName(QString::fromUtf8("hboxLayout"));
    hbox->setSpacing(6);
    //hbox->setMargin(1);

    QSpacerItem *spacerItem = new QSpacerItem(41, 24, QSizePolicy::Expanding, QSizePolicy::Minimum);
    hbox->addItem(spacerItem);

    QPushButton *okButton = new QPushButton("OK", this);
    okButton->setObjectName(QString::fromUtf8("okButton"));
    hbox->addWidget(okButton);

    QPushButton *cancelButton = new QPushButton("&Cancel", this);
    cancelButton->setObjectName(QString::fromUtf8("cancelButton"));
    hbox->addWidget(cancelButton);

    grid->addWidget(m_form, 0, 0);
    grid->addLayout(hbox, 1, 0);

    connect(cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    connect(okButton,     SIGNAL(clicked()), this, SLOT(accept()));
}

ModalFrame::~ModalFrame()
{
}

void ModalFrame::accept()
{
    if(m_form)
        m_form->commit();
    QDialog::accept();
}

void ModalFrame::done(int r)
{
    if(m_form)
    {
        grid->removeWidget(m_form);
        m_form->setParent(m_formParent);
    }
    QDialog::done(r);
}

//  --------------------------------------------------------------------------
//

bool AfxForm::isPropertyDeclared(const QString &name) const
{
    const QMetaObject *meta = metaObject();
    return meta->indexOfProperty(name.toLatin1()) >= meta->propertyOffset();
}

QVariantMap AfxForm::values() const
{
    QVariantMap map;

    const QMetaObject *meta = metaObject();
    for(int i=meta->propertyOffset();i < meta->propertyCount();++i)
    {
        const QMetaProperty &p = meta->property(i);
        const char *name = p.name();
        map.insert/*Multi*/(name, property(name));
    }

    //map.unite(m_dict);

    return map;
}

QVariant AfxForm::value(const QString &key, bool /*lookUp*/) const
{
    if(isPropertyDeclared(key))
        return property(key.toLatin1());

    if(m_dict.contains(key))
        return m_dict[key];

    /*if(lookUp)
    {
        QList<AfxForm *> s = siblings();
        foreach(const AfxForm *f, s)
        {
            if(f != this)
            {
                const QVariant &v = f->value(key, false);
                if(v.isValid())
                    return v;
            }
        }
    }*/

    return QVariant();
}

void AfxForm::setValue(const QString &key, const QVariant &value)
{
    if(isPropertyDeclared(key))
        setProperty(key.toLatin1(), value);
    else
        m_dict.insert/*Multi*/(key, value);
}

int AfxForm::exec()
{
    ModalFrame *frame = new ModalFrame(this);
    int code = frame->exec();
    frame->deleteLater();
    return code;
}

//  end-of-file --------------------------------------------------------------
