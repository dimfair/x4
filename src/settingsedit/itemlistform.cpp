#include "itemlistform.h"
#include "ui_itemlistform.h"

#include <QStringListModel>

ItemListForm::ItemListForm(QWidget *parent)
    : AfxForm(parent),
      ui(std::make_unique<Ui::ItemListForm>())
{
    ui->setupUi(this);

    ui->listView->setModel(new QStringListModel(this));

    connect(ui->addButton, SIGNAL(clicked()), this, SLOT(addItem()));
    connect(ui->removeButton, SIGNAL(clicked()), this, SLOT(removeItem()));
}

ItemListForm::~ItemListForm() = default;

int ItemListForm::currentIndex() const
{
    const QModelIndex &index = ui->listView->currentIndex();
    return index.row();
}

QString ItemListForm::labelText() const
{
    return ui->groupBox->title();
}

void ItemListForm::setLabelText(const QString &text)
{
    ui->groupBox->setTitle(text);
}

QVariantList ItemListForm::values() const
{
    QVariantList values;
    auto *model = this->model();
    for(int r=0;r < model->rowCount();r++)
        values.push_back(model->data(model->index(r, 0), Qt::EditRole));
    return values;
}

void ItemListForm::setValues(const QVariantList &values)
{
    auto *model = this->model();
    for(int n=0;n < values.size();n++)
    {
        if(model->insertRow(model->rowCount()))
        {
            auto const index = model->index(model->rowCount() - 1, 0);
            model->setData(index, values[n]);
        }
    }
}

QAbstractItemModel *ItemListForm::model() const
{
    return ui->listView->model();
}

void ItemListForm::setDelegate(QAbstractItemDelegate *deleg)
{
    ui->listView->setItemDelegate(deleg);
}

void ItemListForm::addItem()
{
    auto *model = this->model();
    if(model->insertRow(model->rowCount()))
    {
        auto const index = model->index(model->rowCount() - 1, 0);
        ui->listView->setCurrentIndex(index);
        ui->listView->edit(index);
    }
}

void ItemListForm::removeItem()
{
    auto const index = ui->listView->currentIndex();
    auto *model = this->model();

    model->removeRow(index.row());
}
