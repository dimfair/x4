
#ifndef __QFX_ITEMLISTFORM_H_200608031412
#define __QFX_ITEMLISTFORM_H_200608031412

#include "form.h"

#include <QAbstractItemModel>

#include <memory>

class QAbstractItemDelegate;

class QAbstractListModel;
namespace Ui {
    class ItemListForm;
}

class ItemListForm : public AfxForm
{
    Q_OBJECT

    Q_PROPERTY(QVariantList values READ values WRITE setValues)

private:
    std::unique_ptr<Ui::ItemListForm> ui;

public:
    ItemListForm(QWidget *parent = 0);
   ~ItemListForm() override;

    QString labelText() const;
    void setLabelText(const QString &);

    int currentIndex() const;

    QVariantList values() const;
    void setValues(const QVariantList &newValues);

    QAbstractItemModel *model() const;

    void setDelegate(QAbstractItemDelegate *);

protected slots:
    virtual void addItem();
    virtual void removeItem();
};


#endif  // __QFX_ITEMLISTFORM_H_200608031412
