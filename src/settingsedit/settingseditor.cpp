
#include <QDebug>

#include "settingseditor.h"
#include "form.h"

AfxSettingsDialog::AfxSettingsDialog(QWidget *parent)
    : QDialog(parent)
{
    m_prevItem = 0;

    ui.setupUi(this);
    ui.splitter->setSizes(QList<int>() << 80 << 240 );
    ui.treeWidget->setHeaderLabels(QStringList() << "Component");

    connect(ui.treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this, SLOT(onTreeItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)));
}

static
QTreeWidgetItem *findItem(QTreeWidgetItem *item)
{
    if(item && item->data(0, Qt::UserRole).isNull())
    {
        QTreeWidgetItem *child = nullptr;
        for(int n=0;!child && n < item->childCount();++n)
            child = findItem(item->child(n));
        return child;
    }

    return item;
}

void AfxSettingsDialog::onTreeItemChanged(QTreeWidgetItem *current, QTreeWidgetItem * /*prev*/)
{
    if(QTreeWidgetItem *item = findItem(current); item)
    {
        if(m_prevItem)
        {
            QFont font = m_prevItem->font(0);
            font.setBold(false);
            m_prevItem->setFont(0, font);
        }
        m_prevItem = item;

        QFont font = item->font(0);
        font.setBold(true);
        item->setFont(0, font);

        selectPage(item->data(0, Qt::UserRole).toString());

        while(item)
        {
            //if(item->childCount())
            //    ui.treeWidget->setItemExpanded(item, true);
            item = item->parent();
        }
    }
}

void AfxSettingsDialog::selectPage(const QString &pageTitle)
{
    if(m_pageIndices.contains(pageTitle))
    {
        //qDebug() << pageTitle << ": index " << m_pageIndices[pageTitle];
        ui.stackedWidget->setCurrentIndex(m_pageIndices[pageTitle]);
    }
}

void AfxSettingsDialog::accept()
{
    for(int n=0;n < ui.stackedWidget->count();++n)
        if(auto *f = qobject_cast<AfxForm *>(ui.stackedWidget->widget(n)))
            f->commit();

    QDialog::accept();
}

QTreeWidgetItem *AfxSettingsDialog::createTreeItem(const QStringList &category, QTreeWidgetItem *parentItem)
{
    int depth = 0;
    QTreeWidgetItem *item = parentItem;
    while(item)
    {
        item = item->parent();
        depth++;
    }

    if(category.size() == depth + 1)
    {
        QTreeWidgetItem *item = parentItem ? new QTreeWidgetItem(parentItem) : new QTreeWidgetItem(ui.treeWidget);
        item->setText(0, category[depth]);
        item->setExpanded(true);
        return item;
    }

    const QString &text = category[depth];
    int childCount = parentItem ? parentItem->childCount() : ui.treeWidget->topLevelItemCount() ;
    for(int n=0;n < childCount;++n)
    {
        QTreeWidgetItem *item = parentItem ? parentItem->child(n) : ui.treeWidget->topLevelItem(n) ;
        if(item->text(0) == text)
            return createTreeItem(category, item);
    }

    item = parentItem ? new QTreeWidgetItem(parentItem) : new QTreeWidgetItem(ui.treeWidget);
    item->setText(0, category[depth]);
    return createTreeItem(category, item);
}

int AfxSettingsDialog::addPage(QWidget *page, const QString &pageTitle)
{
    QString title = pageTitle;
    if(title.isEmpty())
        title = page->windowTitle();
    if(title.isEmpty())
        title = "Page " + QString::number(count() + 1);

    if(QTreeWidgetItem *item = createTreeItem(title.split("/")); item)
    {
        ui.treeWidget->expandAll();

        item->setData(0, Qt::DecorationRole, page->windowIcon());
        item->setData(0, Qt::UserRole, title);

		int index = ui.stackedWidget->addWidget(page);
        m_pageIndices[title] = index;
		return index;
    }

	return -1;
}

int AfxSettingsDialog::count() const
{
	return ui.stackedWidget->count();
}

QWidget *AfxSettingsDialog::page(int index) const
{
	return ui.stackedWidget->widget(index);
}

QWidget *AfxSettingsDialog::page(const QString &title) const
{
	return page(m_pageIndices.value(title));
}


//	-------------------------------------------------------------------------------------------------------------------
