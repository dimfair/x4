
#include <QApplication>
#include <QPluginLoader>
#include <QMainWindow>

#include <afx/IWorkspace>
#include <afx/plugins/Logger>
#include <afx/plugins/PluginMgr>
#include <afx/plugins/SettingsEdit>

Q_IMPORT_PLUGIN( Logger )
Q_IMPORT_PLUGIN( PluginMgr )
Q_IMPORT_PLUGIN( SettingsEditor )
//Q_IMPORT_PLUGIN( RichText )

int main(int argc, char *argv[])
{
    QApplication::setApplicationName("Afx MDI Application");

    QApplication a(argc, argv);
    auto *ws = afx::createMdiWorkspace(&a);

    ws->loadPlugins();

    ws->show(afx::MainWindowLayout::HamburgerMenu);
    return a.exec();
}
