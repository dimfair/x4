include (../plugins.pri)

TEMPLATE  = lib
CONFIG   += staticlib
QT       += widgets

SOURCES += \
    richtext.cpp \
    richtextdoc.cpp \
    richtextview.cpp

HEADERS += \
    richtext.h \
    richtextdoc.h \
    richtextview.h

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
