#ifndef SPREADSHEET_H_202309141633
#define SPREADSHEET_H_202309141633

#include <QObject>
#include <afx/IPlugin>
#include <afx/IDocumentFactory>
#include <afx/IDocumentSerializer>


class RichTextEditor
  : public QObject,
    public afx::IPlugin,
    public afx::IDocumentFactory,
    public afx::IDocumentSerializer
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.xvier-project.Plugins.RichText" FILE "RichText.json")
    Q_INTERFACES( afx::IPlugin afx::IDocumentFactory afx::IDocumentSerializer )

public:
    RichTextEditor(QObject * = {});
   ~RichTextEditor() = default;

    // afx::IPlugin overrides
    QString name() const override;
    QString description() const override;
    bool initialize() override;
    void loadState(QSettings *) override;
    void saveState(QSettings *) const override;

    // afx::IDocumentFactory overrides
    QString factoryId() const override;
    QStringList documentMonikers() const override;
    QObject *createDocument(const QString &moniker = {}) const override;
    QWidget *createEditor(const QString &moniker, QWidget *) const override;

    // afx::IDocumentSerializer overrides
    QString filterText(const QString &moniker) const override;
    bool canRead(const QString &moniker) const override;
    bool canWrite(const QString &moniker) const override;
    int read (QObject *, QIODevice *, const QString &) const override;
    int write(QObject *, QIODevice *, const QString &) const override;
};

#endif // SPREADSHEET_H_202309141633
