#include "richtext.h"
#include "richtextdoc.h"
#include "richtextview.h"

#include <QIODevice>

static constexpr char plainText[] = "Plain text:text/plain";
static constexpr char htmlText[] = "Formatted text:text/html";

RichTextEditor::RichTextEditor(QObject *parent)
    : QObject(parent)
{
}

QString RichTextEditor::name() const
{
    return "Text";
}

QString RichTextEditor::description() const
{
    return "Text Document Editor";
}

bool RichTextEditor::initialize()
{
    return true;
}

void RichTextEditor::loadState(QSettings *)
{
}

void RichTextEditor::saveState(QSettings *) const
{
}

QString RichTextEditor::factoryId() const
{
    return "6167700b-953e-4af7-8de4-d92ecac529ac";
}

QStringList RichTextEditor::documentMonikers() const
{
    return { plainText, htmlText };
}

QObject *RichTextEditor::createDocument(const QString &moniker) const
{
    if(moniker == plainText || moniker == htmlText)
        return new RichTextDocument;
    return {};
}

int RichTextEditor::read(QObject *obj, QIODevice *dev, const QString &) const
{
    if(auto *doc = qobject_cast<RichTextDocument *>(obj))
    {
        auto *textDoc = doc->textDocument();
        textDoc->setHtml(dev->readAll());
        textDoc->setModified(false);
    }

    return 0;
}

int RichTextEditor::write(QObject *obj, QIODevice *dev, const QString &) const
{
    if(auto *doc = qobject_cast<RichTextDocument *>(obj))
    {
        auto *textDoc = doc->textDocument();
        dev->write(textDoc->toHtml().toUtf8());
        return 0;
    }
    return 1;
}

QWidget *RichTextEditor::createEditor(const QString &, QWidget *parent) const
{
    return new RichTextView(parent);
}

QString RichTextEditor::filterText(const QString &moniker) const
{
    if(moniker == htmlText)
        return tr("Formatted text (*.htm)");
    if(moniker == plainText)
        return tr("Plain text (*.txt)");
    return {};
}

bool RichTextEditor::canRead(const QString &moniker) const
{
    return moniker == plainText || moniker == htmlText;
}

bool RichTextEditor::canWrite(const QString &moniker) const
{
    return moniker == plainText || moniker == htmlText;
}
