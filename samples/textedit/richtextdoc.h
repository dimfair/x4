#ifndef SPREADSHEETDOCUMENT_H
#define SPREADSHEETDOCUMENT_H

#include <QObject>

#include <afx/Connection>
#include <afx/IDocument>

class QAbstractItemModel;
class QTextDocument;

class RichTextDocument : public QObject, public afx::IDocument
{
    Q_OBJECT
    Q_INTERFACES( afx::IDocument )

public:
    explicit RichTextDocument(QObject *parent = nullptr);

    QTextDocument *textDocument() const;

    // afx::IDocument overrides
    QString name() const override;
    QString mime() const override;

    bool isClean() const override;
    void setClean(bool = true) override;
    afx::Connection cleanChanged(CleanChengeNotify &&) override;

private:
    void onModificationChanged(bool);

    afx::Response<CleanChengeNotify> m_cleanChangeNotify;
    QTextDocument *m_textDoc;
};

inline QTextDocument *RichTextDocument::textDocument() const
{
    return m_textDoc;
}

#endif // SPREADSHEETDOCUMENT_H
