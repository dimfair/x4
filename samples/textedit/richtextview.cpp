#include "richtextview.h"
#include "richtextdoc.h"

#include <afx/IWorkspace>

#include <QKeySequence>

RichTextView::RichTextView(QWidget *parent)
    : QTextEdit{parent}
{
}

void RichTextView::initialize(QObject *obj)
{
    Q_ASSERT(qobject_cast<RichTextDocument *>(obj));
    if(auto *doc = qobject_cast<RichTextDocument *>(obj))
    {
        m_doc = obj;
        setDocument(doc->textDocument());
    }
}

QObject *RichTextView::document() const
{
    return m_doc;
}

QAction *RichTextView::editClearAction()
{
    if(!m_clearAction)
        m_clearAction = afx::workspace()->createAction(ActionEditClear, this).text("Clear")
                            .connect(this, &QTextEdit::clear);
    return m_clearAction;
}

QAction *RichTextView::editCopyAction()
{
    if(!m_copyAction)
    {
        m_copyAction = afx::workspace()->createAction(ActionEditCopy, this).text("&Copy")
                           .shortcut(QKeySequence::Copy).connect(this, &QTextEdit::copy).disable();
        connect(this, &QTextEdit::copyAvailable,  m_copyAction, &QAction::setEnabled);
    }
    return m_copyAction;
}

QAction *RichTextView::editCutAction()
{
    if(!m_cutAction)
    {
        m_cutAction = afx::workspace()->createAction(ActionEditCut, this).text("Cu&t")
                          .shortcut(QKeySequence::Cut).connect(this, &QTextEdit::cut).disable();
        connect(this, &QTextEdit::copyAvailable,  m_cutAction, &QAction::setEnabled);
    }
    return m_cutAction;
}

QAction *RichTextView::editPasteAction()
{
    if(!m_pasteAction)
        m_pasteAction = afx::workspace()->createAction(ActionEditPaste, this).text("&Paste")
                            .shortcut(QKeySequence::Paste).connect(this, &QTextEdit::paste);
    return m_pasteAction;
}

QAction *RichTextView::editSelectAllAction()
{
    if(!m_selectAllAction)
        m_selectAllAction = afx::workspace()->createAction(ActionEditSelectAll, this).text("Select &All")
                                .shortcut(QKeySequence::SelectAll).connect(this, &QTextEdit::selectAll);
    return m_selectAllAction;
}

QList<QAction *> RichTextView::editAuxActions()
{
    return {};
}

QAction *RichTextView::editUndoAction()
{
    if(!m_undoAction)
    {
        m_undoAction = afx::workspace()->createAction(ActionEditUndo, this).text("&Undo")
                           .shortcut(QKeySequence::Undo).connect(this, &QTextEdit::undo).disable();
        connect(this, &QTextEdit::undoAvailable, m_undoAction, &QAction::setEnabled);
    }
    return m_undoAction;
}

QAction *RichTextView::editRedoAction()
{
    if(!m_redoAction)
    {
        m_redoAction = afx::workspace()->createAction(ActionEditRedo, this).text("&Redo")
                           .shortcut(QKeySequence::Redo).connect(this, &QTextEdit::redo).disable();
        connect(this, &QTextEdit::redoAvailable,  m_redoAction, &QAction::setEnabled);
    }
    return m_redoAction;
}
