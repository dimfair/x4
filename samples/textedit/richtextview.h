#ifndef SPREADSHEETVIEW_H
#define SPREADSHEETVIEW_H

#include <QTextEdit>
#include <afx/IDocumentEditor>

class RichTextView : public QTextEdit, public afx::IDocumentEditor
{
    Q_OBJECT
    Q_INTERFACES( afx::IDocumentEditor )

public:
    explicit RichTextView(QWidget *parent = nullptr);

    void initialize(QObject *) override;
    QObject *document() const override;

    QAction *editUndoAction() override;
    QAction *editRedoAction() override;
    QAction *editClearAction() override;
    QAction *editCopyAction() override;
    QAction *editCutAction() override;
    QAction *editPasteAction() override;
    QAction *editSelectAllAction() override;
    QList<QAction *> editAuxActions() override;

private:
    QObject *m_doc{};

    QAction *m_clearAction{};
    QAction *m_copyAction{};
    QAction *m_cutAction{};
    QAction *m_pasteAction{};
    QAction *m_selectAllAction{};
    QAction *m_undoAction{};
    QAction *m_redoAction{};
};

#endif // SPREADSHEETVIEW_H
