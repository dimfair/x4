#include "richtextdoc.h"

#include <QTextDocument>

RichTextDocument::RichTextDocument(QObject *parent)
    : QObject{parent},
      m_textDoc(new QTextDocument(this))
{
    connect(m_textDoc, &QTextDocument::modificationChanged, this, &RichTextDocument::onModificationChanged);
}

QString RichTextDocument::name() const
{
    return "Text";
}

QString RichTextDocument::mime() const
{
    return "text/*";
}

bool RichTextDocument::isClean() const
{
    return !m_textDoc->isModified();
}

void RichTextDocument::setClean(bool f)
{
    m_textDoc->setModified(f);
}

afx::Connection RichTextDocument::cleanChanged(CleanChengeNotify &&notify)
{
    auto conn = afx::make_connection(notify);
    m_cleanChangeNotify = conn;
    return conn;
}

void RichTextDocument::onModificationChanged(bool f)
{
    afx::call_response(m_cleanChangeNotify, f);
}
